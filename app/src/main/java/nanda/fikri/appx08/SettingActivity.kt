package nanda.fikri.appx08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.activity_setting.edJudul

class SettingActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSimpan -> {
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(FIELD_TEXT, edJudul.text.toString())
                prefEditor.putInt(FIELD_FONT_SIZE, sbFontTitle.progress)
                prefEditor.putString(BG_TITLE, bgTitle)
                prefEditor.putString(COLOR_TITLE, colorTitle)
                prefEditor.putString(FIELD_TEXT_ISI, edIsi.text.toString())
                prefEditor.putInt(FIELD_FONT_SIZE_ISI, sbFontDetail.progress)
                prefEditor.commit()
                Toast.makeText(this, "Perubahan Telah Disimpan", Toast.LENGTH_SHORT).show()
            }
        }


    }

    val arrayBackground = arrayOf("Blue", "Yellow", "Green", "Black")
    lateinit var adapterSpin : ArrayAdapter<String>

    lateinit var preferences:SharedPreferences

    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE = 12
    val DEF_TEXT = ""
    val BG_TITLE = "bg_title"
    val DEF_BG_TITLE = "GREEN"
    val COLOR_TITLE = "color_title"
    val DEF_COLOR_TITLE = "RED"
    val FIELD_FONT_SIZE_ISI = "font_size_isi"
    val FIELD_TEXT_ISI = "teks_isi"
    val DEF_FONT_SIZE_ISI = 12
    val DEF_TEXT_ISI = "Disney Mulan Adalah Film"

    var bgTitle:String = ""
    var colorTitle:String = ""

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
            when(seekbar?.id){
                R.id.sbFontTitle ->{
                    edJudul.setTextSize(progress.toFloat())
                }
                R.id.sbFontDetail ->{
                    edIsi.setTextSize(progress.toFloat())
                }
            }

        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        rg.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.rbBlue -> colorTitle = "BLUE"
                R.id.rbYellow -> colorTitle ="YELLOW"
                R.id.rbGreen -> colorTitle = "GREEN"
                R.id.rbBlack -> colorTitle ="BLACK"
            }
        }

        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1,arrayBackground)
        sp.adapter = adapterSpin

        sp.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                bgTitle = adapterSpin.getItem(position).toString()

            }


        }

        var rgcl = preferences.getString(COLOR_TITLE, DEF_COLOR_TITLE)
        if(rgcl == "BLUE"){
            rg.check(R.id.rbBlue)
        } else if(rgcl == "YELLOW"){
            rg.check(R.id.rbYellow)
        } else if(rgcl == "GREEN"){
            rg.check((R.id.rbGreen))
        } else if(rgcl == "BLACK"){
            rg.check((R.id.rbBlack))
        }

        var bgsp = adapterSpin.getPosition(preferences.getString(BG_TITLE, DEF_BG_TITLE))
        sp.setSelection(bgsp, true)

        edJudul.setText(preferences.getString(FIELD_TEXT, DEF_TEXT))
        edJudul.textSize = preferences.getInt(FIELD_FONT_SIZE, DEF_FONT_SIZE).toFloat()
        sbFontTitle.progress = preferences.getInt(FIELD_FONT_SIZE, DEF_FONT_SIZE)
        sbFontTitle.setOnSeekBarChangeListener(onSeek)

        edIsi.setText(preferences.getString(FIELD_TEXT_ISI, DEF_TEXT_ISI))
        edIsi.textSize = preferences.getInt(FIELD_FONT_SIZE_ISI, DEF_FONT_SIZE_ISI).toFloat()
        sbFontDetail.progress = preferences.getInt(FIELD_FONT_SIZE_ISI, DEF_FONT_SIZE_ISI)
        sbFontDetail.setOnSeekBarChangeListener(onSeek)

        btnSimpan.setOnClickListener(this)
    }
}