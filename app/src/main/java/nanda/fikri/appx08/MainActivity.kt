package nanda.fikri.appx08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var preferences: SharedPreferences

    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE = 12
    val DEF_TEXT = ""
    val BG_TITLE = "bg_title"
    val DEF_BG_TITLE = "GREEN"
    val COLOR_TITLE = "color_title"
    val DEF_COLOR_TITLE = "RED"
    val FIELD_FONT_SIZE_ISI = "font_size_isi"
    val FIELD_TEXT_ISI = "teks_isi"
    val DEF_FONT_SIZE_ISI = 12
    val DEF_TEXT_ISI = "Disney Mulan Adalah Film"

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.ItemSetting ->{
                var intent = Intent(this, SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



    }

    override fun onStart() {
        super.onStart()
        tampil()
    }

    fun tampil(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        Judul.setText(preferences.getString(FIELD_TEXT, DEF_TEXT))
        Judul.textSize = preferences.getInt(FIELD_FONT_SIZE, DEF_FONT_SIZE).toFloat()
        Judul.setBackgroundColor(Color.parseColor(preferences.getString(BG_TITLE, DEF_BG_TITLE)))
        Judul.setTextColor(Color.parseColor(preferences.getString(COLOR_TITLE, DEF_COLOR_TITLE)))
        Isi.setText(preferences.getString(FIELD_TEXT_ISI, DEF_TEXT_ISI))
        Isi.textSize = preferences.getInt(FIELD_FONT_SIZE_ISI, DEF_FONT_SIZE_ISI).toFloat()

    }
}
